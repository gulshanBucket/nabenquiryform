package dataFiles;
/**
 * @author reach
 * This class will hold the data for Google Search test execution project.
 * It has getter and setter function only.
 */

public class ProjectDataClass {
	
	private String url = "https://www.nab.com.au/";
	private static ProjectDataClass dataInstance = null;
	
	private String loanType = "New Home Loan";
	private String existingCustomer = "No";
	private String firstName = "CustomerfirstName";
	private String lastName = "CustomerlastName";
	private String state = "NSW";
	private String phoneNumber = "0469871838";
	private String customerEmail = "gulshanku82@gmail.com";
	
	/**
	 * Method to get class object
	 * @return
	 */
	public static ProjectDataClass getInstance(){
		if(ProjectDataClass.dataInstance==null){
			dataInstance = new ProjectDataClass();
		}
		return dataInstance;
	}

	/**
	 * Method to set class object
	 * 
	 */
	public static void setInstance(ProjectDataClass dataInstance){
		ProjectDataClass.dataInstance = dataInstance;
	}

	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getLoanType() {
		return loanType;
	}
	
	public void setLoanType(String loanType) {
		this.loanType = loanType;
	}
	
	public String getExistingCustomer() {
		return existingCustomer;
	}
	
	public void setExistingCustomer(String existingCustomer) {
		this.existingCustomer = existingCustomer;
	}
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getState() {
		return state;
	}
	
	public void setState(String state) {
		this.state = state;
	}
	
	public String getPhoneNumber() {
		return phoneNumber;
	}
	
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	public String getCustomerEmail() {
		return customerEmail;
	}
	
	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}	
	
}

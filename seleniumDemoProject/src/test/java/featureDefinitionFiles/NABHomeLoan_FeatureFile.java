package featureDefinitionFiles;

import java.util.List;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import util.DataRow;

public class NABHomeLoan_FeatureFile {
	
	
	private NABHomeLoan_KeywordFile keywords = null;
		
	public NABHomeLoan_FeatureFile(){
		keywords= new NABHomeLoan_KeywordFile();
	}
	
	@Given("^The user has launched \"([^\"]*)\" browser in \"([^\"]*)\"$")
	public void the_user_has_launched_browser_in(String BrowserName, String platform) throws Throwable {
		keywords.launchDesiredBrowser(BrowserName,platform);
	}

		
	@Given("^The customer raises an enquiry for \"(.*?)\" with NAB$")
	public void the_customer_raises_an_enquiry_for_with_NAB(String loanType) throws Throwable {
		keywords.navigateToLoanPage(loanType);
	}

	@When("^The customer completes the contact details on contact details form$")
	public void the_customer_completes_the_contact_details_on_contact_details_form(List<DataRow> retrieveInput) throws Throwable {
		keywords.completeCustomerDetails(retrieveInput);
	}

	@Then("^The customer should be able to submit the contact form$")
	public void the_customer_should_be_able_to_submit_the_contact_form() throws Throwable {
		keywords.submitEnquiryForm();
	}
	
}


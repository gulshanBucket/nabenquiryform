package featureDefinitionFiles;

import java.net.MalformedURLException;
import java.util.List;

import org.junit.Assert;
import dataFiles.ProjectDataClass;
import pageObjectsFiles.NABHomeLoanPageObjects;
import util.CommonMethods;
import util.DataRow;

public class NABHomeLoan_KeywordFile{
	private NABHomeLoanPageObjects nabHomeLoanPage = null;;
	private ProjectDataClass  dataClass = null;
	private CommonMethods comm = null;
	
	
	public NABHomeLoan_KeywordFile(){
		
		nabHomeLoanPage = new NABHomeLoanPageObjects();
		dataClass = ProjectDataClass.getInstance();
		comm = new CommonMethods();
		
	}
	
	/**
	 * Method to launch Browser
	 * @throws MalformedURLException 
	 */
	public void launchDesiredBrowser(String BrowserName,String platform) throws MalformedURLException{
		comm.launchBrowser(BrowserName,platform);
	}
	
	/**
	 * Method to navigate to desired application home Page
	 * @throws Exception 
	 */
	public void navigateToLoanPage(String loanType) throws Exception{
		comm.launchURL(dataClass.getUrl());
		
		comm.getScreenshot("Application_HomePage");
		
		navigateToLoanEnquiryPage();
		
		switch (loanType.toUpperCase()) {
		case "NEW HOME LOAN":
			//nabHomeLoanPage.selectHomeloanRadioBtn();
			break;
			
		case "REFINANCING TO NAB":
			//Add the relevant methods
			break;
			
			// Add other cases as per the options available
			
		default:
			Assert.fail("No match found for the home loan type "+loanType+" in the application.");
		}
		
		
	}
	
	/**
	 * @throws Exception 
	 * 
	 */
	public void navigateToLoanEnquiryPage() throws Exception{
		nabHomeLoanPage.clickOnPersonalTab();
		nabHomeLoanPage.clickOnHomeLoanOption();
		if(nabHomeLoanPage.isHomeLoanSubOptiondisplayed())
			nabHomeLoanPage.clickOnHomeLoanSubOption();
		nabHomeLoanPage.clickOnEnquirenewLoanLink();
		comm.getScreenshot("EnquiryPage");
	}
	
		
	/**
	 * Method to set data for the item
	 * @param retrieveInput
	 */
	public void retrieveData(List<DataRow> retrieveInput){
		
		int dataTableSize = retrieveInput.size();		
		if(dataTableSize>0)
		{
			for (DataRow field : retrieveInput) 
			{
				String entity = field.getEntity().toUpperCase();
				String value= field.getValue();
				System.out.println(entity + ":"+value);

				switch(entity.toUpperCase()){
				case "EXISTING CUSTOMER"		:		dataClass.setExistingCustomer(value);		    break;
				case "FIRSTNAME"				:		dataClass.setFirstName(value);		    		break;
				case "LASTNAME"					:		dataClass.setLastName(value);		    		break;
				case "STATE"					:		dataClass.setState(value);		   				break;
				case "PHONENUMBER"				:		dataClass.setPhoneNumber(value);		    	break;
				case "EMAIL"					:		dataClass.setCustomerEmail(value);				break;
				}		
			}		
		}
		
}
	
	/**
	 * Method to complete the input details in the NAB CONTACT CENTRE CALL BACK FORM
	 * @param retrieveInput
	 * @throws Exception
	 */
	public void completeCustomerDetails(List<DataRow> retrieveInput) throws Exception{
		retrieveData(retrieveInput);
		
		nabHomeLoanPage.selectHomeloanRadioBtn();
		nabHomeLoanPage.clickNextBtn();
		
		nabHomeLoanPage.clickOnExistingCustomer(dataClass.getExistingCustomer());
		nabHomeLoanPage.inputCustomerFirstName(dataClass.getFirstName());
		nabHomeLoanPage.inputCustomerLastName(dataClass.getLastName());
		nabHomeLoanPage.selectCustomerState(dataClass.getState());
		nabHomeLoanPage.inputCustomerPhoneNumber(dataClass.getPhoneNumber());
		nabHomeLoanPage.inputCustomerEmail(dataClass.getCustomerEmail());
		
		comm.getScreenshot("CustomerDetails");
	}
	
	
	/**
	 * @throws Exception 
	 * 
	 */
	public void submitEnquiryForm() throws Exception{
		
		//Commenting this piece to avoid submitting the form
		
		//nabHomeLoanPage.clickOnSubmitBtn();
		
		comm.getScreenshot("EnquiryForSubmitted");
		
		//can add any verification to be done after submitting the form, any message that we get on successful submission
		
		// switching back to parent window
		//nabHomeLoanPage.switchToParentWindow();
	}
	
	
	/**
	 * The method will close the Browser after the test execution
	 * @throws Exception 
	 */
	public void closeBrowser() throws Exception{
		comm.getScreenshot("FinalScreenshot");
		comm.teardown();
	}
	
}


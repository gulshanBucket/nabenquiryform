package pageObjectsFiles;


import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import util.SuperClass;


public class NABHomeLoanPageObjects extends SuperClass{
	
	private By personalTab = By.xpath("//li[1]/a[text()='Personal']");
	private By homeLoanOption = By.xpath("//li[5]/a/span[text()='Home loans']");
	private By homeLoanSubOption = By.xpath("//*[starts-with(@id,'insert_')] //li[1]/a/span[text()='Home loans']");

	private By enquireHomeLoanLink = By.linkText("Enquire about a new loan");
	private By shadowrootID = By.cssSelector("#contact-form-shadow-root");
	private By newHomeLoanRadioBtn = By.cssSelector("#myRadioButton-0 > label > span");
	private By NextoBtn = By.cssSelector("#main-container > div > div.sc-ifAKCX.Col__StyledCol-o7bhp7-0.ibULtI > section > div.sc-bdVaJa.iAQrVS > button");
	
	private By existingCustomerNo = By.cssSelector("#field-page-Page1-isExisting > label:nth-child(2)") ;
	private By existingCustomerYes = By.cssSelector("#field-page-Page1-isExisting > label:nth-child(1)") ;
	
	private By customerFirstName = By.cssSelector("#field-page-Page1-aboutYou-firstName");
	private By customerLastName = By.cssSelector("#field-page-Page1-aboutYou-lastName");
	private By customerState =  By.xpath("//*[@id='page-Page1-aboutYou-state']/div/div/div/div[1]/div");
	
	private By customerPhoneNumber = By.cssSelector("#field-page-Page1-aboutYou-phoneNumber");
	private By customerEmail = By.cssSelector("#field-page-Page1-aboutYou-email");
	private By submitBtn = By.cssSelector("#page-Page1-btnGroup-submitBtn");
	
	private ArrayList<String> windowHandles = null;
		
	/**
	 * Method to click on personal tab 
	 * @throws InterruptedException 
	 */
	public void clickOnPersonalTab() throws InterruptedException{
		waitforElementClickable(personalTab);
		clickElement(personalTab);
		shortWait();
	}
	
	/**
	 * Method to click on Home Loan Option
	 * @throws InterruptedException 
	 */
	public void clickOnHomeLoanOption() throws InterruptedException{
		waitforElementClickable(homeLoanOption);
		shortWait();
		clickElement(homeLoanOption);
	}
	
	/**
	 * Method to click on Home Loan sub Option 
	 * @throws InterruptedException 
	 */
	public void clickOnHomeLoanSubOption() throws InterruptedException{
		waitforElementClickable(homeLoanSubOption);
		clickElement(homeLoanSubOption);
	}
	
	/**
	 * Method to check if Home Loan sub Option is displayed 
	 * @throws InterruptedException 
	 */
	public boolean isHomeLoanSubOptiondisplayed() throws InterruptedException{
		return isElementDisplayed(homeLoanSubOption);
	}
	
	/**
	 * Method to click on enquire about loan link
	 * @throws InterruptedException 
	 */
	public void clickOnEnquirenewLoanLink() throws InterruptedException{
		waitforElementClickable(enquireHomeLoanLink);
		clickElement(enquireHomeLoanLink);
	}
	
	
	/**
	 * Method to click on New Home loan radio button
	 * @throws InterruptedException 
	 */
	public void selectHomeloanRadioBtn() throws InterruptedException{
		WebElement element = expandRootElement(shadowrootID);
		element.findElement(newHomeLoanRadioBtn).click();
	}
	
	/**
	 * Method to click on New Home loan radio button
	 * @throws InterruptedException 
	 */
	public void clickNextBtn() throws InterruptedException{
		WebElement element = expandRootElement(shadowrootID);
		scrollToElement(element.findElement(NextoBtn));
		element.findElement(NextoBtn).click();
	}
		
		
	/**
	 * Method to select existing Customer
	 * @throws InterruptedException 
	 */
	public void clickOnExistingCustomer(String value) throws InterruptedException{
		windowHandles = getCurrentWindowHandle();
		switchToWindow(windowHandles.get(1));
		waitforElementClickable(existingCustomerYes);
		if(value.toUpperCase().equals("YES"))
			clickElement(existingCustomerYes);
		else
			clickElement(existingCustomerNo);
	}
	
	/**
	 * Method to input customer first name
	 * @param value
	 * @throws InterruptedException
	 */
	public void inputCustomerFirstName(String value) throws InterruptedException{
		inputValueInElement(customerFirstName, value);
	}
	
	/**
	 * Method to input customer last name
	 * @param value
	 * @throws InterruptedException
	 */
	public void inputCustomerLastName(String value) throws InterruptedException{
		inputValueInElement(customerLastName, value);
	}
	
	public void selectCustomerState(String value) throws InterruptedException{
		selectValueUsingActionClass(customerState);	 
	}
	
	/**
	 * Method to input customer Phone number
	 * @param value
	 * @throws InterruptedException
	 */
	public void inputCustomerPhoneNumber(String value) throws InterruptedException{
		inputValueInElement(customerPhoneNumber, value);
	}
	
	
	
	/**
	 * Method to input customer Email
	 * @param value
	 * @throws InterruptedException
	 */
	public void inputCustomerEmail(String value) throws InterruptedException{
		inputValueInElement(customerEmail, value);
	}
	
	
	/**
	 * Method to click on Submit button
	 * @throws InterruptedException
	 */
	public void clickOnSubmitBtn() throws InterruptedException{
		clickElement(submitBtn);
	}
	
	/**
	 * Switch to parent window
	 */
	public void switchToParentWindow(){
		switchToWindow(windowHandles.get(0));
	}
	

}


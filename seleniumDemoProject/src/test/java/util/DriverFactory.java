package util;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class DriverFactory {
	
	private final Map<String, Supplier<WebDriver>> driverMap = new HashMap<>();
	    
	    
	DriverFactory() {
	 //add all the drivers into a map
	 driverMap.put("Chrome", chromeDriverSupplier);
	 driverMap.put("Firefox", firefoxDriverSupplier);

	}

    /**
     * Chrome driver supplier
     * Please update the URL of the Chrome Driver for your local execution
     */
    public final Supplier<WebDriver> chromeDriverSupplier = () -> {       
	  System.setProperty("webdriver.chrome.driver", "C:\\Users\\reach\\Selenium\\chromedriver_win32\\chromedriver.exe");
    	//System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
	  return new ChromeDriver();
	};

	/**
	 * Firefox driver supplier
	 * Please update the URL of the FireFox (Geckodriver) Driver for your local execution
	 */
	public final Supplier<WebDriver> firefoxDriverSupplier = () -> {
	   System.setProperty("webdriver.gecko.driver", "C:\\Users\\reach\\Selenium\\FireFoxWebDriverUse\\geckodriver.exe");
	   return new FirefoxDriver();
	};

	  
	
	/**
	 * The method returns the desired type
	 * @param type
	 * @return
	 */
	//return a new driver from the map
	 public final WebDriver getDriver(String desiredDriver){
	    return driverMap.get(desiredDriver).get();
	}
	 
	}


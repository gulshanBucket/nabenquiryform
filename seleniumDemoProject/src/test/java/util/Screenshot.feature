Feature: NABHomeLoanEnquiry
  NAB Home Loan Enquiry

  Background: 
    Given The user has launched "chrome" browser in "local Machine"

  @NAB_HOME_LOAN_ENQUIRY
  Scenario Outline: NAB Home Loan Enquiry
    Given The customer raises an enquiry for "<LOAN TYPE>" with NAB
    When The customer completes the contact details on contact details form
      | Entity            | Value                 |
      | Existing Customer | No                    |
      | FirstName         | Gulshan               |
      | LastName          | Kumar                 |
      | State             | NSW                   |
      | PhoneNumber       |            0469871838 |
      | Email             | gulshanku82@gmail.com |
    Then The customer should be able to submit the contact form

    Examples: 
      | LOAN TYPE     |
      | New home Loan |
      
      
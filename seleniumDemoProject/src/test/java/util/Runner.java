package util;


import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;


	@RunWith(Cucumber.class)
	@CucumberOptions(
			features = "src/test/java/util/Screenshot.feature"
			,glue={"featureDefinitionFiles","util"}
	)


	public class Runner {

	}

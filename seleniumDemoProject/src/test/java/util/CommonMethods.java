package util;


import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.google.common.io.Files;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import dataFiles.ProjectDataClass;

/**
 * @author reach
 * This class contains the common methods to be used in the framework
 */

public class CommonMethods extends DriverFactory{
		
	private static WebDriver driver=null;
	private DriverFactory driverFactory= new DriverFactory();
	
	/**
	 * This method initiates a browser instance for test execution
	 * @param browserType
	 * @return 
	 */
    @Before
    public static WebDriver getDriver(){
    	return driver;
    }
	
    /**
     * Method to close the browser after the test execution
     */
    @After()
    public void teardown(){
    	driver.quit();
        driver=null;
        ProjectDataClass.setInstance(null);
    }
    
    /**
     * Method to launch Browser
     * @throws MalformedURLException 
     */
    public void launchBrowser(String browserName,String platform) throws MalformedURLException{
    	
    	switch (browserName.toUpperCase()) {
		case "CHROME":
			if(platform.equalsIgnoreCase("local machine"))
				driver = driverFactory.getDriver("Chrome");
			else
			{
				DesiredCapabilities dc = DesiredCapabilities.chrome();
				//Change the selenium grid accessibility for Docker execution
		        driver = new RemoteWebDriver(new URL("http://192.168.99.100:4444/wd/hub"), dc);
			}
				
			break;

		case "FIREFOX":
			if(platform.equalsIgnoreCase("local machine"))
				driver = driverFactory.getDriver("Firefox");
			else{

				DesiredCapabilities dc = DesiredCapabilities.firefox();
				//Change the selenium grid accessibility for Docker execution
		        driver = new RemoteWebDriver(new URL("http://192.168.99.100:4444/wd/hub"), dc);
			
			}
				
			break;
		}
        driver.manage().window().maximize();
        SuperClass.setDriver(driver);
    }
    
    /**
     * Method to launch URL
     */
    public void launchURL(String url){
    	driver.get(url);
    }
  
    /**
     * Method to take screenshots
     * @param fileName
     * @throws Exception
     */
    public void getScreenshot(String fileName) throws Exception 
    {
            File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
            Files.copy(scrFile, new File("C:\\Screenshot\\"+fileName+".png"));
    }
}


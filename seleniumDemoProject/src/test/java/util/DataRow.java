package util;

public class DataRow {
	private String entity;
	private String field;
	private String value;
	private String key;
	
	public DataRow(String entity, String field, String value) {
		this.entity = entity.trim();
		this.field = field.trim();
		this.value = value.trim();
	}

	public DataRow(String entity, String field, String value, String key) {
		this.entity = entity.trim();
		this.field = field.trim();
		this.value = value.trim();
		this.key = key.trim();
	}
	
	public String getEntity() {
		return entity.trim().toUpperCase();
	}

	public String getField() {
		return field.trim().toUpperCase();
	}

	public String getValue() {
		this.value = this.value.replaceAll(",", "");
		this.value = this.value.replaceAll("\\$", "");
		return value.trim();
	}
	public String getValueWithSpecialChar() {
			return value.trim();
	}
	
	public String getKey() {
		this.key = this.key.replaceAll(",", "");
		this.key = this.key.replaceAll("\\$", "");
		return key.trim();
	}

	@Override
	public String toString() {
		return "Field [entity=" + entity + "; field=" + field + "; value="
				+ value + "]";
	}
}

